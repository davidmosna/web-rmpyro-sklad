// Require plugins
var gulp = require('gulp');

var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var cssGlobbing = require('gulp-css-globbing');
var cssnano = require('gulp-cssnano');
var livereload = require('gulp-livereload');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();
var imageResize = require('gulp-image-resize');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');

// Global config
var config = {

    bower: [
        'web/vendor/bower_components/'
    ],
    node_js: [
        'node_modules/'
    ],
    dist: [
        'web/'
    ],
    root: [
        'src/AppBundle/Resources/'
    ]

};

// Set paths for further use
var paths = {

    dependencies_styles: [
        config.bower + "bootstrap/dist/css/bootstrap.min.css",
        config.bower + "bootstrap-social/bootstrap-social.css"
    ],
    styles: [
        config.root + 'scss/**/*.scss',
        config.root + 'scss/modules/**/*.scss'
    ],
    dependencies: [
        config.root + 'js/vendor/jquery.print.js',
        config.root + 'js/vendor/prettydate.js'
    ],
    scripts: [
        config.root + 'js/*.js'
    ],
    fonts: [
        config.root + 'fonts/**/*.{ttf,woff,woff2,eot,svg,otf}'
    ],
    images: [
        config.root + 'img/**/*',
        '!' + config.root + 'img/logos/*'
    ],
    logos: [
        config.root + 'img/logos/*'
    ],
    svg: [
        config.root + 'svg/**/*'
    ],
    dest: [
        config.dist
    ]

};


// Merge and compile scss styles
gulp.task('styles', function(){
    return gulp.src(paths.styles)
        .pipe(plumber())
        .pipe(cssGlobbing({
            extensions: ['.scss'],
            autoReplaceBlock: {
                onOff: true,
                globBlockBegin: 'cssGlobbingBegin',
                globBlockEnd: 'cssGlobbingEnd',
                globBlockContents: 'modules/*.scss'
            },
            scssImportPath: {
                leading_underscore: false,
                filename_extension: false
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            errLogToConsole: true
        }).on('error', notify.onError(function (error) {
            return 'Problem file : ' + error.message;
        })))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dest + 'css'))
        .pipe(browserSync.stream());

});


// Merge and compile scss styles
gulp.task('styles-prod', function(){
    return gulp.src(paths.styles)
        .pipe(plumber())
        .pipe(cssGlobbing({
            extensions: ['.scss'],
            autoReplaceBlock: {
                onOff: true,
                globBlockBegin: 'cssGlobbingBegin',
                globBlockEnd: 'cssGlobbingEnd',
                globBlockContents: 'modules/*.scss'
            },
            scssImportPath: {
                leading_underscore: false,
                filename_extension: false
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            errLogToConsole: true
        }).on('error', notify.onError(function (error) {
            return 'Problem file : ' + error.message;
        })))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dest + 'css'));

});



// Take all js scripts, compile and copy
gulp.task('js', function(){

    var scripts = paths.dependencies.slice();
    scripts = scripts.concat(paths.scripts);

    return gulp.src(scripts)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dest + 'js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest + 'js'));
});


// Copy fonts
gulp.task('fonts', function(){
    return gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.dest + 'fonts'));
});


gulp.task('logos', function(){
    gulp.src(paths.logos)
        .pipe(imageResize({
            width : 200,
            height : 80,
            crop : false,
            upscale : false
        }))
        .pipe(gulp.dest(paths.dest + 'img/logos'));
});


gulp.task('images', function(){
    return gulp.src(paths.images)
        .pipe(plumber())
        .pipe(imageResize({
            width : 600,
            height : 600,
            crop : true,
            upscale : false
        }))
        .pipe(gulp.dest(paths.dest + 'img'));
});


gulp.task('svg', function(){
    return gulp.src(paths.svg)
        .pipe(plumber())
        .pipe(gulp.dest(paths.dest + 'svg'));
});


// Clean everything from destination
gulp.task('clean', function(){
    return del([
        paths.dest + 'css',
        paths.dest + 'js',
        paths.dest + 'img',
        paths.dest + 'fonts',
        paths.dest + 'html',
        paths.dest + 'svg'
    ]);
});



// Watch
gulp.task('serve', ['styles', 'js', 'fonts', 'images', 'logos', 'svg'], function(){

    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.scripts, ['js']);
    gulp.watch(paths.fonts, ['fonts']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.svg, ['svg']);

    // Create LiveReload server
    livereload.listen();

    // Watch any files in dist/, reload on change
    gulp.watch([paths.dest]).on('change', livereload.changed);

});

// default (dev) task
gulp.task('dev', ['serve', 'styles', 'js', 'fonts', 'images', 'logos', 'svg']);
gulp.task('default', function(){
    runSequence('clean', 'dev');
});


// generate production data
gulp.task('prod', ['styles-prod', 'js', 'fonts', 'logos', 'svg', 'images']);
gulp.task('build', function(){
    runSequence('clean', 'prod');
});

