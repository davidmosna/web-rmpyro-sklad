# Sklad

##Deployment

### Client

* cd /../sklad_lts/
* scp -P 1022 -r ./* lama28_marketingpyro.cz@pinetree.cz:/home/lama28_marketingpyro.cz/websites/marketingpyro.cz/sklad.data
* ssh lama28_marketingpyro.cz@pinetree.cz -p 1022

### Server

* ./composer.phar install
* php app/console cache:clear --env=prod --no-debug

