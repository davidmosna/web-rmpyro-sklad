<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Manufacturer;

class ProductRepository extends EntityRepository {

	public function findProductByBarcode($codename, $codetype, $codeformat){

		return $this->getEntityManager()
				    ->getRepository("AppBundle:Product")
					->findOneBy(array("codename" => $codename, "codetype" => $codetype, "codeformat" => $codeformat));
	}

	public function findProductsByManufacturer(Manufacturer $manufacturer){

		return $this->getEntityManager()
			        ->getRepository("AppBundle:Product")
					->findBy(array("manufacturer" => $manufacturer));

	}

	public function findProductsCount(){

		$repo = $this->getEntityManager()->getRepository("AppBundle:Product");
		$query = $repo->createQueryBuilder('p')
			->select('SUM(p.pieces)')
			->getQuery();

		return $query->getSingleScalarResult();
	}

}
