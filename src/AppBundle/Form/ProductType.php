<?php

namespace AppBundle\Form;

use AppBundle\Form\DataTransformer\ManufacturerTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{

	private $manager;

	public function __construct(ObjectManager $manager)
	{
		$this->manager = $manager;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('codename')
			->add('codetype')
			->add('codeformat')
			->add('caliber')
			->add('pieces')
			->add('shots')
			->add('name')
			->add('manufacturer', TextType::class)
			->add('purchaseprice')
			->add('salesprice')
			->add('description')
			->add('save', SubmitType::class);

		$builder->get('manufacturer')
			->addModelTransformer(new ManufacturerTransformer($this->manager));

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Product'
		));
	}
}
