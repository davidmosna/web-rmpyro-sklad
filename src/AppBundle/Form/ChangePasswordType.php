<?php
namespace AppBundle\Form;

use AppBundle\Entity\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('oldPassword', 'password', array('label' => ChangePassword::PASSWORD_LABEL_OLD));
		$builder->add('newPassword', 'repeated', array(
			'type' => 'password',
			'invalid_message' => ChangePassword::PASSWORD_FLASH_FIELDS_MUST_BE_SAME,
			'required' => true,
			'first_options' => array('label' => ChangePassword::PASSWORD_LABEL_NEW),
			'second_options' => array('label' => ChangePassword::PASSWORD_LABEL_NEW_REPEAT)
		));

		$builder->add('save', SubmitType::class, array('label' => ChangePassword::PASSWORD_BUTTON_SUBMIT));

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\ChangePassword'
		));
	}


}