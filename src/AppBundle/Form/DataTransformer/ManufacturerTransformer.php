<?php
namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Manufacturer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ManufacturerTransformer implements DataTransformerInterface
{
	private $manager;

	public function __construct(ObjectManager $manager)
	{
		$this->manager = $manager;
	}

	/**
	 * Transforms an object to a string.
	 */
	public function transform($manufacturer)
	{
		if (null === $manufacturer) {
			return '';
		}

		return $manufacturer->getName();
	}

	/**
	 * Transforms a string to an object.
	 */
	public function reverseTransform($name)
	{

		if (!$name) {
			return;
		}

		$repository = $this->manager->getRepository('AppBundle:Manufacturer');
		$manufacturer = $repository->findOneBy(array("name" => $name));

		if (null === $manufacturer) {
			$manufacturer = new Manufacturer();
			$manufacturer->setName($name);
			$this->manager->persist($manufacturer);
		}

		return $manufacturer;
	}
}