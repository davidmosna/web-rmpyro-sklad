$(function () {

    $(".inline-label__input").each(function (index, element) {
        var $input = $(element);
        var $label = $(this).next();
        var $parent = $(this).parent().eq(0);
        var $class = "js-filled-input";

        if ($input.val().length == 0) {
            $label.removeClass($class);
        }

        $parent.on("click", function () {
            $input.focus();
        });

        $input.on("focus", function (event) {
            $label.addClass($class);
        });

        $input.on("blur change", function (e) {
            if ($input.val().length == 0) {
                $label.removeClass($class);
            } else {
                $label.addClass($class);
            }
        });
    });

});