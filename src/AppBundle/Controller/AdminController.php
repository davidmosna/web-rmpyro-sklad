<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\ChangePassword;

use AppBundle\Form\ChangePasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;


/**
 * Admin controller.
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{
	/**
	 * Show dashboard.
	 *
	 * @Route("/", name="admin_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
		return $this->redirectToRoute("product_index");
	}


	/**
	 * Change password.
	 *
	 * @Route("/change-password", name="admin_change_password")
	 */
	public function changePasswordAction(Request $request)
	{
		$changePasswordModel = new ChangePassword();
		$form = $this->createForm(new ChangePasswordType(), $changePasswordModel);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$pass = $form->getData()->getNewPassword();
			$user = $this->getUser();
			$encoder = $this->container->get('security.password_encoder');
			$encoded = $encoder->encodePassword($user, $pass);
			$user->setPassword($encoded);
			$this->getDoctrine()->getManager()->flush($user);

			$this->addFlash("success", ChangePassword::PASSWORD_CHANGE_SUCCESS);
			return $this->redirect($this->generateUrl("admin_change_password"));
		}

		return $this->render("admin/change-password.html.twig", array('form' => $form->createView()));

	}

}
