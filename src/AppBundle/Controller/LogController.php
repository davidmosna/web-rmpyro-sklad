<?php

namespace AppBundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * Product controller.
 *
 * @Route("/admin/log")
 */
class LogController extends Controller
{
	/**
	 * @Route("/", name="log_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
		$logEntries = $this->get('app.log')->getAll();
		return $this->render("log/index.html.twig", array("logEntries" => $logEntries));
	}

}
