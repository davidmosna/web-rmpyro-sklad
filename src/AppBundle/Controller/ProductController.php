<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Product controller.
 *
 * @Route("/admin/products")
 */
class ProductController extends Controller
{
	/**
	 * Lists all Product entities.
	 *
	 * @Route("/", name="product_index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
		return $this->render("products/index.html.twig");
	}


	/**
	 * @Route("/detail/by-code/{codename}/{codetype}/{codeformat}",
	 *     name="product_show_by_code",
	 *     defaults={"codename" = null, "codetype" = null, "codeformat" = null})
	 */
	public function showByCodeAction($codename, $codetype, $codeformat)
	{

		$arr = array(
			'codename' => $codename,
			'codetype' => $codetype,
			'codeformat' => $codeformat
		);

		$res = $this->getDoctrine()->getRepository("AppBundle:Product")->findOneBy($arr);

		if ($res) {
			return $this->redirectToRoute('product_show', array("id" => $res->getId()));
		} else {
			$this->addFlash("info", Product::PRODUCT_NOT_EXISTING);
			return $this->redirectToRoute('product_new', $arr);
		}

	}


	/**
	 * Finds and displays a Product entity.
	 *
	 * @Route("/detail/{id}", name="product_show", defaults={"id" = null})
	 * @Method({"GET", "POST"})
	 */
	public function showAction(Product $product, Request $request)
	{

		$orig = $this
			->getDoctrine()
			->getManager()
			->getRepository("AppBundle:Product")
			->findOneBy(array("id" => $product->getId()))
			->getPieces();

		$form = $this->createForm(ProductType::class, $product);
		$deleteForm = $this->createDeleteForm($product);
		$form->handleRequest($request);
		$diff = ($product->getPieces() < $orig) ? $orig - $product->getPieces() : 0;

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$taken = $product->getTaken();
			$product->setTaken($taken + $diff);
			$em->persist($product);
			$em->flush();

			$this->addFlash("success", Product::PRODUCT_UPDATE_SUCCESS);
			$this->get('app.log')->add(
				array(
					"type" => Log::ACTION_UPDATE,
					"message" => $product->getName()
				)
			);

			return $this->redirectToRoute('product_show', array("id" => $product->getId()));
		}

		return $this->render("products/show.html.twig", array(
			'product' => $product,
			'form' => $form->createView(),
			'delete_form' => $deleteForm->createView()
		));

	}


	/**
	 * Deletes a Product entity.
	 *
	 * @Route("/delete/{id}", name="product_delete")
	 */
	public function deleteAction(Request $request, Product $product)
	{

		$form = $this->createDeleteForm($product);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$name = $product->getName();
			$em->remove($product);
			$em->flush();
			$this->addFlash("success", Product::PRODUCT_DELETE_SUCCESS);
			$this->get('app.log')->add(
				array(
					"type" => Log::ACTION_DELETE,
					"message" => $name
				)
			);
		}

		return $this->redirectToRoute('product_index');
	}


	/**
	 * Creates a form to delete a Product entity.
	 *
	 * @param Product $product The Product entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm(Product $product)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('product_delete', array('id' => $product->getId())))
			->setMethod('DELETE')
			->getForm();
	}


	/**
	 * Creates a new Product entity.
	 *
	 * @Route("/new/{codename}/{codetype}/{codeformat}",
	 *     name="product_new",
	 *     defaults={"codename" = null, "codetype" = null, "codeformat" = null})
	 * @Method({"GET", "POST"})
	 */
	public function newAction(Request $request, $codename, $codetype, $codeformat)
	{
		$product = new Product();

		$product->setCodename($codename);
		$product->setCodetype($codetype);
		$product->setCodeformat($codeformat);


		$form = $this->createForm(ProductType::class, $product);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$datetime = new \DateTime("now");
			$product->setTaken(0);
			$product->setModified($datetime);
			$product->setCreated($datetime);
			$product->setAuthor($this->getUser());
			$em = $this->getDoctrine()->getManager();
			$em->persist($product);
			$em->flush();
			$this->addFlash("success", Product::PRODUCT_ADD_SUCCESS);
			$this->get('app.log')->add(
				array(
					"type" => Log::ACTION_ADD,
					"message" => $product->getName()
				)
			);
		}

		return $this->render("products/new.html.twig", array(
			'product' => $product,
			'form' => $form->createView(),
		));
	}

}
