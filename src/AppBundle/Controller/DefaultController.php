<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;


/**
 * Default controller.
 *
 */
class DefaultController extends Controller
{
	/**
	 * Redirect to dashboard.
	 *
	 * @Route("/", name="index")
	 * @Method("GET")
	 */
	public function indexAction()
	{
		return $this->redirectToRoute("product_index");
	}


}
