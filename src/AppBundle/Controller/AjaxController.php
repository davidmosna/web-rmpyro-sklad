<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Config;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use JMS\SerializerBundle\JMSSerializerBundle;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/admin/ajax")
 */
class AjaxController extends FOSRestController
{

	protected $serializer;

	public function __construct()
	{
		$this->serializer = SerializerBuilder::create()->build();
	}


	public static function array_values_recursive($ary)
	{
		$lst = array();
		foreach (array_keys($ary) as $k) {
			$v = $ary[$k];
			if (is_scalar($v) || is_null($v)) {
				$lst[] = $v;
			} elseif (is_array($v)) {
				$lst = array_merge($lst,
					self::array_values_recursive($v)
				);
			}
		}
		return $lst;
	}


	/**
	 * @Route("/config", name="ajax_user_config")
	 */
	public function configAction(Request $request)
	{

		$method = $request->getMethod();
		$data = $request->getContent();
		$user_config = $this->getUser()->getConfig();

		switch ($method) {
			case "GET":
				$json = $this->serializer->serialize($user_config, "json");
				return new Response(
					$json,
					200,
					array(
						'Content-Type' => 'application/json; charset=utf-8'
					)
				);

			case "POST":
				$object = $this->serializer->deserialize(
					$data,
					'AppBundle\Entity\Config',
					'json'
				);

				$obj = (!$user_config) ? new Config() : $user_config;

				$obj->setColumnsproducts($object->getColumnsproducts());
				$obj->setColumnsmanufacturers($object->getColumnsmanufacturers());
				$this->getUser()->setConfig($obj);
				$em = $this->getDoctrine()->getManager();
				$em->persist($obj);
				$em->flush();

				return new Response(null, 200);

			default:
				return new Response(null, 405); // method not allowed
		}

	}

	/**
	 * @Route("/productsCount", name="ajax_products_count")
	 */
	public function productsCountAction()
	{
		return new Response(
			$this->getDoctrine()
				->getRepository("AppBundle:Product")
				->findProductsCount()
		);
	}

	/**
	 * @Route("/productsCountGroupedByManufacturers", name="ajax_products_count_grouped_by_manufacturers")
	 */
	public function productsCountGroupedByManufacturersAction()
	{

		$em = $this->getDoctrine()->getEntityManager();

		$query = $em->createQueryBuilder()
			->select("sum(p.pieces) as value, (m.name) as label")
			->from('AppBundle:Product', 'p')
			->leftJoin("AppBundle:Manufacturer", "m", "WITH", "p.manufacturer = m.id")
			->groupBy("p.manufacturer")
			->having("value>0")
			->getQuery();


		return new Response(
			json_encode($query->getResult()),
			200,
			array(
				'Content-Type' => 'application/json; charset=utf-8'
			)
		);

	}

	/**
	 * @Route("/productsList", name="ajax_products_list")
	 */
	public function productsListAction()
	{

		$products = $this->getDoctrine()->getRepository("AppBundle:Product")->findAll();
		$context = new SerializationContext();
		$context->setSerializeNull(true);
		$context->setGroups(array('group1'));

		$json = $this->serializer->serialize(
			$products,
			'json',
			$context
		);

		$array = json_decode($json, true);
		$result = array();
		foreach ($array as $obj) {
			$result[] = self::array_values_recursive($obj);
		}

		return new Response(
			json_encode(array("data" => $result)),
			200,
			array(
				'Content-Type' => 'application/json; charset=utf-8'
			)
		);
	}


	/**
	 * @Route("/autocomplete/{entityType}", name="ajax_autocomplete")
	 */
	public function autocompleteAction(Request $request, $entityType = null)
	{
		$names = array();
		$term = trim(strip_tags($request->get('term')));

		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository('AppBundle:' . $entityType)->createQueryBuilder('c')
			->where('c.name LIKE :name')
			->setParameter('name', '%' . $term . '%')
			->getQuery()
			->getResult();

		foreach ($entities as $entity) {
			$names[] = $entity->getName();
		}

		$response = new JsonResponse();
		$response->setData($names);

		return $response;

	}
}
