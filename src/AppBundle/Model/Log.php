<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;


class Log
{

	private $em;

	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
	}

	public function add(array $args)
	{

		$log = new \AppBundle\Entity\Log();

		foreach ($args as $key => $value) {
			$fn = "set" . ucfirst($key);
			$log->$fn($value);
		}

		$log->setCreated(new \DateTime("now"));
		$this->em->persist($log);
		$this->em->flush();
	}

	public function getAll(){

		return $this
			->em
			->getRepository("AppBundle:Log")
			->findBy(array(), array("id" => "DESC"), 20);

	}



}
