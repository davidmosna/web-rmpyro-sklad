<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Config;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User implements UserInterface, \Serializable
{

	public function __construct()
	{
		$this->products = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getApikey()
	{
		return $this->apikey;
	}

	/**
	 * @param mixed $apikey
	 */
	public function setApikey($apikey)
	{
		$this->apikey = $apikey;
	}

	/**
	 * @return mixed
	 */
	public function getProducts()
	{
		return $this->products;
	}

	/**
	 * @param mixed $products
	 */
	public function setProducts($products)
	{
		$this->products = $products;
	}

	/**
	 * @return mixed
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @param mixed $config
	 */
	public function setConfig(Config $config)
	{
		$this->config = $config;
	}

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=25, unique=true)
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=60, unique=true, nullable=true)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", unique=true, nullable=true)
	 */
	private $apikey;

	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="author")
	 */
	private $products;

	/**
	 * @ORM\OneToOne(targetEntity="Config")
	 * @ORM\JoinColumn(name="config", referencedColumnName="id")
	 */
	private $config;

	/**
	 * @param mixed $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function getSalt()
	{
		return null;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getRoles()
	{
		return array('ROLE_USER');
	}

	public function eraseCredentials()
	{
	}

	/** @see \Serializable::serialize() */
	public function serialize()
	{
		return serialize(array(
			$this->id,
			$this->username,
			$this->email,
			$this->password,
			$this->apikey
		));
	}

	/** @see \Serializable::unserialize() */
	public function unserialize($serialized)
	{
		list (
			$this->id,
			$this->username,
			$this->email,
			$this->password,
			$this->apikey
			) = unserialize($serialized);
	}

}