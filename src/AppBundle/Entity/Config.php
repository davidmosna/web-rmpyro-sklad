<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Config
 *
 * @ORM\Table(name="config")
 * @ORM\Entity
 */
class Config
{
	/**
	 * @return mixed
	 */
	public function getColumnsproducts()
	{
		return $this->columnsproducts;
	}

	/**
	 * @param mixed $columnsproducts
	 */
	public function setColumnsproducts($columnsproducts)
	{
		$this->columnsproducts = $columnsproducts;
	}

	/**
	 * @return mixed
	 */
	public function getColumnsmanufacturers()
	{
		return $this->columnsmanufacturers;
	}

	/**
	 * @param mixed $columnsmanufacturers
	 */
	public function setColumnsmanufacturers($columnsmanufacturers)
	{
		$this->columnsmanufacturers = $columnsmanufacturers;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @ORM\Column(name="columnsProducts", type="json_array", nullable=true)
	 * @Type("array")
	 */
	private $columnsproducts;

	/**
	 * @ORM\Column(name="columnsManufacturers", type="json_array", nullable=true)
	 * @Type("array")
	 */
	private $columnsmanufacturers;

	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

}
