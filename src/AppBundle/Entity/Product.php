<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PostPersist;
use Doctrine\ORM\Mapping\PostLoad;
use JMS\Serializer\Annotation\Groups;
use Doctrine\ORM\Event;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Product
{

	const PRODUCT_DELETE_SUCCESS = "Produkt byl odstraněn";
	const PRODUCT_ADD_SUCCESS = "Produkt byl vytvořen";
	const PRODUCT_UPDATE_SUCCESS = "Produkt byl aktualizován";
	const PRODUCT_NOT_EXISTING = "Tento produkt zatím neexistuje";

	const PRODUCT_DELETE_SUCCESS_LOG = "Odstranění produktu {name}";
	const PRODUCT_ADD_SUCCESS_LOG = "Vytvoření produktu {name}";
	const PRODUCT_UPDATE_SUCCESS_LOG = "Aktualizace produktu {name}";


	/**
	 * @return mixed
	 */
	public function getCodename()
	{
		return $this->codename;
	}

	/**
	 * @param mixed $codename
	 */
	public function setCodename($codename)
	{
		$this->codename = $codename;
	}

	/**
	 * @return mixed
	 */
	public function getCodetype()
	{
		return $this->codetype;
	}

	/**
	 * @param mixed $codetype
	 */
	public function setCodetype($codetype)
	{
		$this->codetype = $codetype;
	}

	/**
	 * @return mixed
	 */
	public function getCodeformat()
	{
		return $this->codeformat;
	}

	/**
	 * @param mixed $codeformat
	 */
	public function setCodeformat($codeformat)
	{
		$this->codeformat = $codeformat;
	}

	/**
	 * @return mixed
	 */
	public function getCaliber()
	{
		return $this->caliber;
	}

	/**
	 * @param mixed $caliber
	 */
	public function setCaliber($caliber)
	{
		$this->caliber = $caliber;
	}

	/**
	 * @return mixed
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param mixed $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return mixed
	 */
	public function getModified()
	{
		return $this->modified;
	}

	/**
	 * @param mixed $modified
	 */
	public function setModified($modified)
	{
		$this->modified = $modified;
	}

	/**
	 * @return mixed
	 */
	public function getPieces()
	{
		return $this->pieces;
	}

	/**
	 * @param mixed $pieces
	 */
	public function setPieces($pieces)
	{
		$this->pieces = $pieces;
	}

	/**
	 * @return mixed
	 */
	public function getShots()
	{
		return $this->shots;
	}

	/**
	 * @param mixed $shots
	 */
	public function setShots($shots)
	{
		$this->shots = $shots;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getPurchaseprice()
	{
		return $this->purchaseprice;
	}

	/**
	 * @param mixed $purchaseprice
	 */
	public function setPurchaseprice($purchaseprice)
	{
		$this->purchaseprice = $purchaseprice;
	}

	/**
	 * @return mixed
	 */
	public function getSalesprice()
	{
		return $this->salesprice;
	}

	/**
	 * @param mixed $salesprice
	 */
	public function setSalesprice($salesprice)
	{
		$this->salesprice = $salesprice;
	}

	/**
	 * @return mixed
	 */
	public function getManufacturer()
	{
		return $this->manufacturer;
	}

	/**
	 * @param mixed $manufacturer
	 */
	public function setManufacturer($manufacturer)
	{
		$this->manufacturer = $manufacturer;
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param mixed $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}

	/**
	 * @return mixed
	 */
	public function getTaken()
	{
		return $this->taken;
	}

	/**
	 * @param mixed $taken
	 */
	public function setTaken($taken)
	{
		$this->taken = $taken;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 * @Groups({"group1"})
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="Manufacturer", inversedBy="products")
	 * @Groups({"group1"})
	 */
	private $manufacturer;

	/**
	 * @ORM\Column(name="pieces", type="integer", nullable=false)
	 * @Groups({"group1"})
	 */
	private $pieces;

	/**
	 * @ORM\Column(name="codeName", type="string", length=255, nullable=true)
	 */
	private $codename;

	/**
	 * @ORM\Column(name="codeType", type="string", length=255, nullable=true)
	 */
	private $codetype;

	/**
	 * @ORM\Column(name="codeFormat", type="string", length=255, nullable=true)
	 */
	private $codeformat;

	/**
	 * @ORM\Column(name="shots", type="integer", nullable=true)
	 * @Groups({"group1"})
	 */
	private $shots;

	/**
	 * @ORM\Column(name="caliber", type="string", nullable=true)
	 * @Groups({"group1"})
	 */
	private $caliber;

	/**
	 * @ORM\Column(name="description", type="string", length=510, nullable=true)
	 * @Groups({"group1"})
	 */
	private $description;

	/**
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	private $created;

	/**
	 * @ORM\Column(name="modified", type="datetime", nullable=false)
	 */
	private $modified;

	/**
	 * @ORM\Column(name="purchasePrice", type="float", nullable=true)
	 * @Groups({"group1"})
	 */
	private $purchaseprice;

	/**
	 * @ORM\Column(name="salesPrice", type="float", nullable=true)
	 * @Groups({"group1"})
	 */
	private $salesprice;

	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="products")
	 */
	private $author;

	/**
	 * @ORM\Column(name="taken", type="integer", nullable=false)
	 */
	private $taken;

	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Groups({"group1"})
	 */
	private $id;

}
