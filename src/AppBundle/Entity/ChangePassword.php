<?php
namespace AppBundle\Entity;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;


class ChangePassword
{

	const PASSWORD_LABEL_OLD = "Staré heslo";
	const PASSWORD_LABEL_NEW = "Nové heslo";
	const PASSWORD_LABEL_NEW_REPEAT = "Opakovat nové heslo";
	const PASSWORD_FLASH_FIELDS_MUST_BE_SAME = "Nové heslo se liší";
	const PASSWORD_BUTTON_SUBMIT = "Změnit heslo";
	const PASSWORD_CHANGE_SUCCESS = "Heslo bylo úspěšně změněno";

	/**
	 * @SecurityAssert\UserPassword(
	 *     message = "Wrong value for your current password"
	 * )
	 */
	protected $oldPassword;


	/**
	 * @Assert\Length(
	 *     min = 6,
	 *     minMessage = "Password should by at least 6 chars long"
	 * )
	 */
	protected $newPassword;

	/**
	 * @return mixed
	 */
	public function getOldPassword()
	{
		return $this->oldPassword;
	}

	/**
	 * @param mixed $oldPassword
	 */
	public function setOldPassword($oldPassword)
	{
		$this->oldPassword = $oldPassword;
	}

	/**
	 * @return mixed
	 */
	public function getNewPassword()
	{
		return $this->newPassword;
	}

	/**
	 * @param mixed $newPassword
	 */
	public function setNewPassword($newPassword)
	{
		$this->newPassword = $newPassword;
	}



}