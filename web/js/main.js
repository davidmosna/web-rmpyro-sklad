/* @license
 * jQuery.print, version 1.5.0
 *  (c) Sathvik Ponangi, Doers' Guild
 * Licence: CC-By (http://creativecommons.org/licenses/by/3.0/)
 *--------------------------------------------------------------------------*/
(function ($) {
    "use strict";
    // A nice closure for our definitions
    function getjQueryObject(string) {
        // Make string a vaild jQuery thing
        var jqObj = $("");
        try {
            jqObj = $(string)
                .clone();
        } catch (e) {
            jqObj = $("<span />")
                .html(string);
        }
        return jqObj;
    }

    function printFrame(frameWindow, content, options) {
        // Print the selected window/iframe
        var def = $.Deferred();
        try {
            frameWindow = frameWindow.contentWindow || frameWindow.contentDocument || frameWindow;
            var wdoc = frameWindow.document || frameWindow.contentDocument || frameWindow;
            if(options.doctype) {
                wdoc.write(options.doctype);
            }
            wdoc.write(content);
            wdoc.close();
            var printed = false;
            function callPrint() {
                if(printed) {
                    return;
                }
                // Fix for IE : Allow it to render the iframe
                frameWindow.focus();
                try {
                    // Fix for IE11 - printng the whole page instead of the iframe content
                    if (!frameWindow.document.execCommand('print', false, null)) {
                        // document.execCommand returns false if it failed -http://stackoverflow.com/a/21336448/937891
                        frameWindow.print();
                    }
                } catch (e) {
                    frameWindow.print();
                }
                frameWindow.close();
                printed = true;
                def.resolve();
            }
            // Print once the frame window loads - seems to work for the new-window option but unreliable for the iframe
            $(frameWindow).on("load", callPrint);
            // Fallback to printing directly if the frame doesn't fire the load event for whatever reason
            setTimeout(callPrint, options.timeout);
        } catch (err) {
            def.reject(err);
        }
        return def;
    }

    function printContentInIFrame(content, options) {
        var $iframe = $(options.iframe + "");
        var iframeCount = $iframe.length;
        if (iframeCount === 0) {
            // Create a new iFrame if none is given
            $iframe = $('<iframe height="0" width="0" border="0" wmode="Opaque"/>')
                .prependTo('body')
                .css({
                    "position": "absolute",
                    "top": -999,
                    "left": -999
                });
        }
        var frameWindow = $iframe.get(0);
        return printFrame(frameWindow, content, options)
            .done(function () {
                // Success
                setTimeout(function () {
                    // Wait for IE
                    if (iframeCount === 0) {
                        // Destroy the iframe if created here
                        $iframe.remove();
                    }
                }, 100);
            })
            .fail(function (err) {
                // Use the pop-up method if iframe fails for some reason
                console.error("Failed to print from iframe", err);
                printContentInNewWindow(content, options);
            })
            .always(function () {
                try {
                    options.deferred.resolve();
                } catch (err) {
                    console.warn('Error notifying deferred', err);
                }
            });
    }

    function printContentInNewWindow(content, options) {
        // Open a new window and print selected content
        var frameWindow = window.open();
        return printFrame(frameWindow, content, options)
            .always(function () {
                try {
                    options.deferred.resolve();
                } catch (err) {
                    console.warn('Error notifying deferred', err);
                }
            });
    }

    function isNode(o) {
        /* http://stackoverflow.com/a/384380/937891 */
        return !!(typeof Node === "object" ? o instanceof Node : o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName === "string");
    }
    $.print = $.fn.print = function () {
        // Print a given set of elements
        var options, $this, self = this;
        // console.log("Printing", this, arguments);
        if (self instanceof $) {
            // Get the node if it is a jQuery object
            self = self.get(0);
        }
        if (isNode(self)) {
            // If `this` is a HTML element, i.e. for
            // $(selector).print()
            $this = $(self);
            if (arguments.length > 0) {
                options = arguments[0];
            }
        } else {
            if (arguments.length > 0) {
                // $.print(selector,options)
                $this = $(arguments[0]);
                if (isNode($this[0])) {
                    if (arguments.length > 1) {
                        options = arguments[1];
                    }
                } else {
                    // $.print(options)
                    options = arguments[0];
                    $this = $("html");
                }
            } else {
                // $.print()
                $this = $("html");
            }
        }
        // Default options
        var defaults = {
            globalStyles: true,
            mediaPrint: false,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 750,
            title: null,
            doctype: '<!doctype html>'
        };
        // Merge with user-options
        options = $.extend({}, defaults, (options || {}));
        var $styles = $("");
        if (options.globalStyles) {
            // Apply the stlyes from the current sheet to the printed page
            $styles = $("style, link, meta, base, title");
        } else if (options.mediaPrint) {
            // Apply the media-print stylesheet
            $styles = $("link[media=print]");
        }
        if (options.stylesheet) {
            // Add a custom stylesheet if given
            $styles = $.merge($styles, $('<link rel="stylesheet" href="' + options.stylesheet + '">'));
        }
        // Create a copy of the element to print
        var copy = $this.clone();
        // Wrap it in a span to get the HTML markup string
        copy = $("<span/>")
            .append(copy);
        // Remove unwanted elements
        copy.find(options.noPrintSelector)
            .remove();
        // Add in the styles
        copy.append($styles.clone());
        // Update title
        if (options.title) {
            var title = $("title", copy);
            if (title.length === 0) {
                title = $("<title />");
                copy.append(title);
            }
            title.text(options.title);
        }
        // Appedned content
        copy.append(getjQueryObject(options.append));
        // Prepended content
        copy.prepend(getjQueryObject(options.prepend));
        if (options.manuallyCopyFormValues) {
            // Manually copy form values into the HTML for printing user-modified input fields
            // http://stackoverflow.com/a/26707753
            copy.find("input")
                .each(function () {
                    var $field = $(this);
                    if ($field.is("[type='radio']") || $field.is("[type='checkbox']")) {
                        if ($field.prop("checked")) {
                            $field.attr("checked", "checked");
                        }
                    } else {
                        $field.attr("value", $field.val());
                    }
                });
            copy.find("select").each(function () {
                var $field = $(this);
                $field.find(":selected").attr("selected", "selected");
            });
            copy.find("textarea").each(function () {
                // Fix for https://github.com/DoersGuild/jQuery.print/issues/18#issuecomment-96451589
                var $field = $(this);
                $field.text($field.val());
            });
        }
        // Get the HTML markup string
        var content = copy.html();
        // Notify with generated markup & cloned elements - useful for logging, etc
        try {
            options.deferred.notify('generated_markup', content, copy);
        } catch (err) {
            console.warn('Error notifying deferred', err);
        }
        // Destroy the copy
        copy.remove();
        if (options.iframe) {
            // Use an iframe for printing
            try {
                printContentInIFrame(content, options);
            } catch (e) {
                // Use the pop-up method if iframe fails for some reason
                console.error("Failed to print from iframe", e.stack, e.message);
                printContentInNewWindow(content, options);
            }
        } else {
            // Use a new window for printing
            printContentInNewWindow(content, options);
        }
        return this;
    };
})(jQuery);
(function (factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as anonymous module.
        define(["jquery"], factory);
    } else {
        // Browser globals.
        factory(jQuery);
    }
})(function ($) {

    "use strict";

    var PrettyDate = function (element, options) {
            options = $.isPlainObject(options) ? options : {};
            this.$element = $(element);
            this.defaults = $.extend({}, PrettyDate.defaults, this.$element.data(), options);
            this.init();
        },

    // Helper variables
        floor = Math.floor,
        second = 1000,
        minute = 60 * second,
        hour = 60 * minute,
        day = 24 * hour,
        week = 7 * day,
        month = 31 * day,
        year = 365 * day;

    PrettyDate.prototype = {
        constructor: PrettyDate,

        init: function () {
            var $this = this.$element,
                defaults = this.defaults,
                isInput = $this.is("input"),
                originalDate = isInput ? $this.val() : $this.text();

            this.isInput = isInput;
            this.originalDate = originalDate;
            this.format = PrettyDate.fn.parseFormat(defaults.dateFormat);
            this.setDate(defaults.date || originalDate);
            this.active = true;

            if (this.date) {
                this.prettify();

                if (defaults.autoUpdate) {
                    this.update();
                }
            }
        },

        setDate: function (date) {
            if (date) {
                this.date = PrettyDate.fn.parseDate(date, this.format);
            }
        },

        prettify: function () {
            var diff = (new Date()).getTime() - this.date.getTime(),
                past = diff > 0 ? true : false,
                messages = this.defaults.messages,
                $this = this.$element,
                prettyDate;

            if (!this.active) {
                return;
            }

            diff = diff < 0 ? (second - diff) : diff;
            prettyDate = (
                diff < 2 * second ? messages.second :
                    diff < minute ? messages.seconds.replace("%s", floor(diff / second)) :
                        diff < 2 * minute ? messages.minute :
                            diff < hour ? messages.minutes.replace("%s", floor(diff / minute)) :
                                diff < 2 * hour ? messages.hour :
                                    diff < day ? messages.hours.replace("%s", floor(diff / hour)) :
                                        diff < 2 * day ? (past ? messages.yesterday : messages.tomorrow) :
                                            diff < 3 * day ? (past ? messages.beforeYesterday : messages.afterTomorrow) :
                                                /* diff < 2 * day ? messages.day : */
                                                diff < week ? messages.days.replace("%s", floor(diff / day)) :
                                                    diff < 2 * week ? messages.week :
                                                        diff < 4 * week ? messages.weeks.replace("%s", floor(diff / week)) :
                                                            diff < 2 * month ? messages.month :
                                                                diff < year ? messages.months.replace("%s", floor(diff / month)) :
                                                                    diff < 2 * year ? messages.year : messages.years.replace("%s", floor(diff / year))
            );


            if (this.isInput) {
                $this.val(prettyDate);
            } else {
                $this.text(prettyDate);
            }

            this.prettyDate = prettyDate;
        },

        destroy: function () {
            var $this = this.$element,
                originalDate = this.originalDate;

            if (!this.active) {
                return;
            }

            if (this.defaults.autoUpdate && this.autoUpdate) {
                clearInterval(this.autoUpdate);
            }

            if (this.isInput) {
                $this.val(originalDate);
            } else {
                $this.text(originalDate);
            }

            $this.removeData("prettydate");

            this.active = false;
        },

        update: function () {
            var duration = this.defaults.duration,
                that = this;

            if (typeof duration === "number" && duration > 0) {
                this.autoUpdate = setInterval(function () {
                    that.prettify();
                }, duration);
            }
        }
    };

    PrettyDate.fn = {
        parseFormat: function (format) {
            var parts = typeof format === "string" ? format.match(/(\w+)/g) : [],
                monthMatched = false;

            if (!parts || parts.length === 0) {
                throw new Error("Invalid date format.");
            }

            format = $.map(parts, function (n) {
                var part = n.substr(0, 1);

                switch (part) {
                    case "S":
                    case "s":
                        part = "s";
                        break;

                    case "m":
                        part = monthMatched ? "m" : "M";
                        monthMatched = true;
                        break;

                    case "H":
                    case "h":
                        part = "h";
                        break;

                    case "D":
                    case "d":
                        part = "D";
                        break;

                    case "M":
                        part = "M";
                        monthMatched = true;
                        break;

                    case "Y":
                    case "y":
                        part = "Y";
                        break;

                    // No default
                }

                return part;
            });

            return format;
        },

        parseDate: function (date, format) {
            var parts = typeof date === "string" ? date.match(/(\d+)/g) : [],
                data = {
                    Y: 0,
                    M: 0,
                    D: 0,
                    h: 0,
                    m: 0,
                    s: 0
                };

            if ($.isArray(parts) && $.isArray(format) && parts.length === format.length) {
                $.each(format, function (i, n) {
                    data[n] = parseInt(parts[i], 10) || 0;
                });

                data.Y += data.Y > 0 && data.Y < 100 ? 2000 : 0; // Year: 14 -> 2014

                date = new Date(data.Y, data.M - 1, data.D, data.h, data.m, data.s);
            } else {
                date = new Date(date);
            }

            return date.getTime() ? date : null;
        }
    };

    PrettyDate.defaults = {
        autoUpdate: false,
        date: null,
        dateFormat: "YYYY-MM-DD hh:mm:ss",
        duration: 60000, // milliseconds
        messages: {
            second: "právě teď",
            seconds: "před %s sekundami",
            minute: "před minutou",
            minutes: "před %s minutami",
            hour: "před hodinou",
            hours: "před %s hodinami",
            day: "včera",
            days: "před %s dny",
            week: "před týdnem",
            weeks: "před %s týdny",
            month: "před měsícem",
            months: "před %s měsíci",
            year: "před rokem",
            years: "před %s lety",

            // Extra
            yesterday: "včera",
            beforeYesterday: "před 2 dny",
            tomorrow: "zítra",
            afterTomorrow: "pozítří"
        }
    };

    PrettyDate.setDefaults = function (options) {
        $.extend(PrettyDate.defaults, options);
    };

    // Register as jQuery plugin
    $.fn.prettydate = function (options, settings) {
        return this.each(function () {
            var $this = $(this),
                data = $this.data("prettydate");

            if (!data) {
                $this.data("prettydate", (data = new PrettyDate(this, options)));
            }

            if (typeof options === "string" && $.isFunction(data[options])) {
                data[options](settings);
            }
        });
    };

    $.fn.prettydate.constructor = PrettyDate;
    $.fn.prettydate.setDefaults = PrettyDate.setDefaults;

    $(function () {
        $("[prettydate]").prettydate();
    });
});

$(function () {

    $(".inline-label__input").each(function (index, element) {
        var $input = $(element);
        var $label = $(this).next();
        var $parent = $(this).parent().eq(0);
        var $class = "js-filled-input";

        if ($input.val().length == 0) {
            $label.removeClass($class);
        }

        $parent.on("click", function () {
            $input.focus();
        });

        $input.on("focus", function (event) {
            $label.addClass($class);
        });

        $input.on("blur change", function (e) {
            if ($input.val().length == 0) {
                $label.removeClass($class);
            } else {
                $label.addClass($class);
            }
        });
    });

});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5wcmludC5qcyIsInByZXR0eWRhdGUuanMiLCJmb3JtLmxhYmVscy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3UUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBAbGljZW5zZVxuICogalF1ZXJ5LnByaW50LCB2ZXJzaW9uIDEuNS4wXG4gKiAgKGMpIFNhdGh2aWsgUG9uYW5naSwgRG9lcnMnIEd1aWxkXG4gKiBMaWNlbmNlOiBDQy1CeSAoaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnkvMy4wLylcbiAqLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgLy8gQSBuaWNlIGNsb3N1cmUgZm9yIG91ciBkZWZpbml0aW9uc1xuICAgIGZ1bmN0aW9uIGdldGpRdWVyeU9iamVjdChzdHJpbmcpIHtcbiAgICAgICAgLy8gTWFrZSBzdHJpbmcgYSB2YWlsZCBqUXVlcnkgdGhpbmdcbiAgICAgICAgdmFyIGpxT2JqID0gJChcIlwiKTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGpxT2JqID0gJChzdHJpbmcpXG4gICAgICAgICAgICAgICAgLmNsb25lKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIGpxT2JqID0gJChcIjxzcGFuIC8+XCIpXG4gICAgICAgICAgICAgICAgLmh0bWwoc3RyaW5nKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4ganFPYmo7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJpbnRGcmFtZShmcmFtZVdpbmRvdywgY29udGVudCwgb3B0aW9ucykge1xuICAgICAgICAvLyBQcmludCB0aGUgc2VsZWN0ZWQgd2luZG93L2lmcmFtZVxuICAgICAgICB2YXIgZGVmID0gJC5EZWZlcnJlZCgpO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgZnJhbWVXaW5kb3cgPSBmcmFtZVdpbmRvdy5jb250ZW50V2luZG93IHx8IGZyYW1lV2luZG93LmNvbnRlbnREb2N1bWVudCB8fCBmcmFtZVdpbmRvdztcbiAgICAgICAgICAgIHZhciB3ZG9jID0gZnJhbWVXaW5kb3cuZG9jdW1lbnQgfHwgZnJhbWVXaW5kb3cuY29udGVudERvY3VtZW50IHx8IGZyYW1lV2luZG93O1xuICAgICAgICAgICAgaWYob3B0aW9ucy5kb2N0eXBlKSB7XG4gICAgICAgICAgICAgICAgd2RvYy53cml0ZShvcHRpb25zLmRvY3R5cGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgd2RvYy53cml0ZShjb250ZW50KTtcbiAgICAgICAgICAgIHdkb2MuY2xvc2UoKTtcbiAgICAgICAgICAgIHZhciBwcmludGVkID0gZmFsc2U7XG4gICAgICAgICAgICBmdW5jdGlvbiBjYWxsUHJpbnQoKSB7XG4gICAgICAgICAgICAgICAgaWYocHJpbnRlZCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIEZpeCBmb3IgSUUgOiBBbGxvdyBpdCB0byByZW5kZXIgdGhlIGlmcmFtZVxuICAgICAgICAgICAgICAgIGZyYW1lV2luZG93LmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gRml4IGZvciBJRTExIC0gcHJpbnRuZyB0aGUgd2hvbGUgcGFnZSBpbnN0ZWFkIG9mIHRoZSBpZnJhbWUgY29udGVudFxuICAgICAgICAgICAgICAgICAgICBpZiAoIWZyYW1lV2luZG93LmRvY3VtZW50LmV4ZWNDb21tYW5kKCdwcmludCcsIGZhbHNlLCBudWxsKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZG9jdW1lbnQuZXhlY0NvbW1hbmQgcmV0dXJucyBmYWxzZSBpZiBpdCBmYWlsZWQgLWh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzIxMzM2NDQ4LzkzNzg5MVxuICAgICAgICAgICAgICAgICAgICAgICAgZnJhbWVXaW5kb3cucHJpbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgZnJhbWVXaW5kb3cucHJpbnQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZnJhbWVXaW5kb3cuY2xvc2UoKTtcbiAgICAgICAgICAgICAgICBwcmludGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBkZWYucmVzb2x2ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gUHJpbnQgb25jZSB0aGUgZnJhbWUgd2luZG93IGxvYWRzIC0gc2VlbXMgdG8gd29yayBmb3IgdGhlIG5ldy13aW5kb3cgb3B0aW9uIGJ1dCB1bnJlbGlhYmxlIGZvciB0aGUgaWZyYW1lXG4gICAgICAgICAgICAkKGZyYW1lV2luZG93KS5vbihcImxvYWRcIiwgY2FsbFByaW50KTtcbiAgICAgICAgICAgIC8vIEZhbGxiYWNrIHRvIHByaW50aW5nIGRpcmVjdGx5IGlmIHRoZSBmcmFtZSBkb2Vzbid0IGZpcmUgdGhlIGxvYWQgZXZlbnQgZm9yIHdoYXRldmVyIHJlYXNvblxuICAgICAgICAgICAgc2V0VGltZW91dChjYWxsUHJpbnQsIG9wdGlvbnMudGltZW91dCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgZGVmLnJlamVjdChlcnIpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBkZWY7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJpbnRDb250ZW50SW5JRnJhbWUoY29udGVudCwgb3B0aW9ucykge1xuICAgICAgICB2YXIgJGlmcmFtZSA9ICQob3B0aW9ucy5pZnJhbWUgKyBcIlwiKTtcbiAgICAgICAgdmFyIGlmcmFtZUNvdW50ID0gJGlmcmFtZS5sZW5ndGg7XG4gICAgICAgIGlmIChpZnJhbWVDb3VudCA9PT0gMCkge1xuICAgICAgICAgICAgLy8gQ3JlYXRlIGEgbmV3IGlGcmFtZSBpZiBub25lIGlzIGdpdmVuXG4gICAgICAgICAgICAkaWZyYW1lID0gJCgnPGlmcmFtZSBoZWlnaHQ9XCIwXCIgd2lkdGg9XCIwXCIgYm9yZGVyPVwiMFwiIHdtb2RlPVwiT3BhcXVlXCIvPicpXG4gICAgICAgICAgICAgICAgLnByZXBlbmRUbygnYm9keScpXG4gICAgICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgIFwicG9zaXRpb25cIjogXCJhYnNvbHV0ZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInRvcFwiOiAtOTk5LFxuICAgICAgICAgICAgICAgICAgICBcImxlZnRcIjogLTk5OVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHZhciBmcmFtZVdpbmRvdyA9ICRpZnJhbWUuZ2V0KDApO1xuICAgICAgICByZXR1cm4gcHJpbnRGcmFtZShmcmFtZVdpbmRvdywgY29udGVudCwgb3B0aW9ucylcbiAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAvLyBTdWNjZXNzXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFdhaXQgZm9yIElFXG4gICAgICAgICAgICAgICAgICAgIGlmIChpZnJhbWVDb3VudCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gRGVzdHJveSB0aGUgaWZyYW1lIGlmIGNyZWF0ZWQgaGVyZVxuICAgICAgICAgICAgICAgICAgICAgICAgJGlmcmFtZS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIDEwMCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmZhaWwoZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgICAgICAgIC8vIFVzZSB0aGUgcG9wLXVwIG1ldGhvZCBpZiBpZnJhbWUgZmFpbHMgZm9yIHNvbWUgcmVhc29uXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhaWxlZCB0byBwcmludCBmcm9tIGlmcmFtZVwiLCBlcnIpO1xuICAgICAgICAgICAgICAgIHByaW50Q29udGVudEluTmV3V2luZG93KGNvbnRlbnQsIG9wdGlvbnMpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5hbHdheXMoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnMuZGVmZXJyZWQucmVzb2x2ZSgpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ0Vycm9yIG5vdGlmeWluZyBkZWZlcnJlZCcsIGVycik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHJpbnRDb250ZW50SW5OZXdXaW5kb3coY29udGVudCwgb3B0aW9ucykge1xuICAgICAgICAvLyBPcGVuIGEgbmV3IHdpbmRvdyBhbmQgcHJpbnQgc2VsZWN0ZWQgY29udGVudFxuICAgICAgICB2YXIgZnJhbWVXaW5kb3cgPSB3aW5kb3cub3BlbigpO1xuICAgICAgICByZXR1cm4gcHJpbnRGcmFtZShmcmFtZVdpbmRvdywgY29udGVudCwgb3B0aW9ucylcbiAgICAgICAgICAgIC5hbHdheXMoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnMuZGVmZXJyZWQucmVzb2x2ZSgpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ0Vycm9yIG5vdGlmeWluZyBkZWZlcnJlZCcsIGVycik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaXNOb2RlKG8pIHtcbiAgICAgICAgLyogaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMzg0MzgwLzkzNzg5MSAqL1xuICAgICAgICByZXR1cm4gISEodHlwZW9mIE5vZGUgPT09IFwib2JqZWN0XCIgPyBvIGluc3RhbmNlb2YgTm9kZSA6IG8gJiYgdHlwZW9mIG8gPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIG8ubm9kZVR5cGUgPT09IFwibnVtYmVyXCIgJiYgdHlwZW9mIG8ubm9kZU5hbWUgPT09IFwic3RyaW5nXCIpO1xuICAgIH1cbiAgICAkLnByaW50ID0gJC5mbi5wcmludCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gUHJpbnQgYSBnaXZlbiBzZXQgb2YgZWxlbWVudHNcbiAgICAgICAgdmFyIG9wdGlvbnMsICR0aGlzLCBzZWxmID0gdGhpcztcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJQcmludGluZ1wiLCB0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICBpZiAoc2VsZiBpbnN0YW5jZW9mICQpIHtcbiAgICAgICAgICAgIC8vIEdldCB0aGUgbm9kZSBpZiBpdCBpcyBhIGpRdWVyeSBvYmplY3RcbiAgICAgICAgICAgIHNlbGYgPSBzZWxmLmdldCgwKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaXNOb2RlKHNlbGYpKSB7XG4gICAgICAgICAgICAvLyBJZiBgdGhpc2AgaXMgYSBIVE1MIGVsZW1lbnQsIGkuZS4gZm9yXG4gICAgICAgICAgICAvLyAkKHNlbGVjdG9yKS5wcmludCgpXG4gICAgICAgICAgICAkdGhpcyA9ICQoc2VsZik7XG4gICAgICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBvcHRpb25zID0gYXJndW1lbnRzWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgLy8gJC5wcmludChzZWxlY3RvcixvcHRpb25zKVxuICAgICAgICAgICAgICAgICR0aGlzID0gJChhcmd1bWVudHNbMF0pO1xuICAgICAgICAgICAgICAgIGlmIChpc05vZGUoJHRoaXNbMF0pKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucyA9IGFyZ3VtZW50c1sxXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vICQucHJpbnQob3B0aW9ucylcbiAgICAgICAgICAgICAgICAgICAgb3B0aW9ucyA9IGFyZ3VtZW50c1swXTtcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMgPSAkKFwiaHRtbFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vICQucHJpbnQoKVxuICAgICAgICAgICAgICAgICR0aGlzID0gJChcImh0bWxcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy8gRGVmYXVsdCBvcHRpb25zXG4gICAgICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIGdsb2JhbFN0eWxlczogdHJ1ZSxcbiAgICAgICAgICAgIG1lZGlhUHJpbnQ6IGZhbHNlLFxuICAgICAgICAgICAgc3R5bGVzaGVldDogbnVsbCxcbiAgICAgICAgICAgIG5vUHJpbnRTZWxlY3RvcjogXCIubm8tcHJpbnRcIixcbiAgICAgICAgICAgIGlmcmFtZTogdHJ1ZSxcbiAgICAgICAgICAgIGFwcGVuZDogbnVsbCxcbiAgICAgICAgICAgIHByZXBlbmQ6IG51bGwsXG4gICAgICAgICAgICBtYW51YWxseUNvcHlGb3JtVmFsdWVzOiB0cnVlLFxuICAgICAgICAgICAgZGVmZXJyZWQ6ICQuRGVmZXJyZWQoKSxcbiAgICAgICAgICAgIHRpbWVvdXQ6IDc1MCxcbiAgICAgICAgICAgIHRpdGxlOiBudWxsLFxuICAgICAgICAgICAgZG9jdHlwZTogJzwhZG9jdHlwZSBodG1sPidcbiAgICAgICAgfTtcbiAgICAgICAgLy8gTWVyZ2Ugd2l0aCB1c2VyLW9wdGlvbnNcbiAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBkZWZhdWx0cywgKG9wdGlvbnMgfHwge30pKTtcbiAgICAgICAgdmFyICRzdHlsZXMgPSAkKFwiXCIpO1xuICAgICAgICBpZiAob3B0aW9ucy5nbG9iYWxTdHlsZXMpIHtcbiAgICAgICAgICAgIC8vIEFwcGx5IHRoZSBzdGx5ZXMgZnJvbSB0aGUgY3VycmVudCBzaGVldCB0byB0aGUgcHJpbnRlZCBwYWdlXG4gICAgICAgICAgICAkc3R5bGVzID0gJChcInN0eWxlLCBsaW5rLCBtZXRhLCBiYXNlLCB0aXRsZVwiKTtcbiAgICAgICAgfSBlbHNlIGlmIChvcHRpb25zLm1lZGlhUHJpbnQpIHtcbiAgICAgICAgICAgIC8vIEFwcGx5IHRoZSBtZWRpYS1wcmludCBzdHlsZXNoZWV0XG4gICAgICAgICAgICAkc3R5bGVzID0gJChcImxpbmtbbWVkaWE9cHJpbnRdXCIpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChvcHRpb25zLnN0eWxlc2hlZXQpIHtcbiAgICAgICAgICAgIC8vIEFkZCBhIGN1c3RvbSBzdHlsZXNoZWV0IGlmIGdpdmVuXG4gICAgICAgICAgICAkc3R5bGVzID0gJC5tZXJnZSgkc3R5bGVzLCAkKCc8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cIicgKyBvcHRpb25zLnN0eWxlc2hlZXQgKyAnXCI+JykpO1xuICAgICAgICB9XG4gICAgICAgIC8vIENyZWF0ZSBhIGNvcHkgb2YgdGhlIGVsZW1lbnQgdG8gcHJpbnRcbiAgICAgICAgdmFyIGNvcHkgPSAkdGhpcy5jbG9uZSgpO1xuICAgICAgICAvLyBXcmFwIGl0IGluIGEgc3BhbiB0byBnZXQgdGhlIEhUTUwgbWFya3VwIHN0cmluZ1xuICAgICAgICBjb3B5ID0gJChcIjxzcGFuLz5cIilcbiAgICAgICAgICAgIC5hcHBlbmQoY29weSk7XG4gICAgICAgIC8vIFJlbW92ZSB1bndhbnRlZCBlbGVtZW50c1xuICAgICAgICBjb3B5LmZpbmQob3B0aW9ucy5ub1ByaW50U2VsZWN0b3IpXG4gICAgICAgICAgICAucmVtb3ZlKCk7XG4gICAgICAgIC8vIEFkZCBpbiB0aGUgc3R5bGVzXG4gICAgICAgIGNvcHkuYXBwZW5kKCRzdHlsZXMuY2xvbmUoKSk7XG4gICAgICAgIC8vIFVwZGF0ZSB0aXRsZVxuICAgICAgICBpZiAob3B0aW9ucy50aXRsZSkge1xuICAgICAgICAgICAgdmFyIHRpdGxlID0gJChcInRpdGxlXCIsIGNvcHkpO1xuICAgICAgICAgICAgaWYgKHRpdGxlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHRpdGxlID0gJChcIjx0aXRsZSAvPlwiKTtcbiAgICAgICAgICAgICAgICBjb3B5LmFwcGVuZCh0aXRsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZS50ZXh0KG9wdGlvbnMudGl0bGUpO1xuICAgICAgICB9XG4gICAgICAgIC8vIEFwcGVkbmVkIGNvbnRlbnRcbiAgICAgICAgY29weS5hcHBlbmQoZ2V0alF1ZXJ5T2JqZWN0KG9wdGlvbnMuYXBwZW5kKSk7XG4gICAgICAgIC8vIFByZXBlbmRlZCBjb250ZW50XG4gICAgICAgIGNvcHkucHJlcGVuZChnZXRqUXVlcnlPYmplY3Qob3B0aW9ucy5wcmVwZW5kKSk7XG4gICAgICAgIGlmIChvcHRpb25zLm1hbnVhbGx5Q29weUZvcm1WYWx1ZXMpIHtcbiAgICAgICAgICAgIC8vIE1hbnVhbGx5IGNvcHkgZm9ybSB2YWx1ZXMgaW50byB0aGUgSFRNTCBmb3IgcHJpbnRpbmcgdXNlci1tb2RpZmllZCBpbnB1dCBmaWVsZHNcbiAgICAgICAgICAgIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI2NzA3NzUzXG4gICAgICAgICAgICBjb3B5LmZpbmQoXCJpbnB1dFwiKVxuICAgICAgICAgICAgICAgIC5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyICRmaWVsZCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgIGlmICgkZmllbGQuaXMoXCJbdHlwZT0ncmFkaW8nXVwiKSB8fCAkZmllbGQuaXMoXCJbdHlwZT0nY2hlY2tib3gnXVwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRmaWVsZC5wcm9wKFwiY2hlY2tlZFwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRmaWVsZC5hdHRyKFwiY2hlY2tlZFwiLCBcImNoZWNrZWRcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZmllbGQuYXR0cihcInZhbHVlXCIsICRmaWVsZC52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvcHkuZmluZChcInNlbGVjdFwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgJGZpZWxkID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICAkZmllbGQuZmluZChcIjpzZWxlY3RlZFwiKS5hdHRyKFwic2VsZWN0ZWRcIiwgXCJzZWxlY3RlZFwiKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29weS5maW5kKFwidGV4dGFyZWFcIikuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgLy8gRml4IGZvciBodHRwczovL2dpdGh1Yi5jb20vRG9lcnNHdWlsZC9qUXVlcnkucHJpbnQvaXNzdWVzLzE4I2lzc3VlY29tbWVudC05NjQ1MTU4OVxuICAgICAgICAgICAgICAgIHZhciAkZmllbGQgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgICRmaWVsZC50ZXh0KCRmaWVsZC52YWwoKSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICAvLyBHZXQgdGhlIEhUTUwgbWFya3VwIHN0cmluZ1xuICAgICAgICB2YXIgY29udGVudCA9IGNvcHkuaHRtbCgpO1xuICAgICAgICAvLyBOb3RpZnkgd2l0aCBnZW5lcmF0ZWQgbWFya3VwICYgY2xvbmVkIGVsZW1lbnRzIC0gdXNlZnVsIGZvciBsb2dnaW5nLCBldGNcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIG9wdGlvbnMuZGVmZXJyZWQubm90aWZ5KCdnZW5lcmF0ZWRfbWFya3VwJywgY29udGVudCwgY29weSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgY29uc29sZS53YXJuKCdFcnJvciBub3RpZnlpbmcgZGVmZXJyZWQnLCBlcnIpO1xuICAgICAgICB9XG4gICAgICAgIC8vIERlc3Ryb3kgdGhlIGNvcHlcbiAgICAgICAgY29weS5yZW1vdmUoKTtcbiAgICAgICAgaWYgKG9wdGlvbnMuaWZyYW1lKSB7XG4gICAgICAgICAgICAvLyBVc2UgYW4gaWZyYW1lIGZvciBwcmludGluZ1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBwcmludENvbnRlbnRJbklGcmFtZShjb250ZW50LCBvcHRpb25zKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAvLyBVc2UgdGhlIHBvcC11cCBtZXRob2QgaWYgaWZyYW1lIGZhaWxzIGZvciBzb21lIHJlYXNvblxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWlsZWQgdG8gcHJpbnQgZnJvbSBpZnJhbWVcIiwgZS5zdGFjaywgZS5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICBwcmludENvbnRlbnRJbk5ld1dpbmRvdyhjb250ZW50LCBvcHRpb25zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIFVzZSBhIG5ldyB3aW5kb3cgZm9yIHByaW50aW5nXG4gICAgICAgICAgICBwcmludENvbnRlbnRJbk5ld1dpbmRvdyhjb250ZW50LCBvcHRpb25zKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xufSkoalF1ZXJ5KTsiLCIoZnVuY3Rpb24gKGZhY3RvcnkpIHtcbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgLy8gQU1ELiBSZWdpc3RlciBhcyBhbm9ueW1vdXMgbW9kdWxlLlxuICAgICAgICBkZWZpbmUoW1wianF1ZXJ5XCJdLCBmYWN0b3J5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBCcm93c2VyIGdsb2JhbHMuXG4gICAgICAgIGZhY3RvcnkoalF1ZXJ5KTtcbiAgICB9XG59KShmdW5jdGlvbiAoJCkge1xuXG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICB2YXIgUHJldHR5RGF0ZSA9IGZ1bmN0aW9uIChlbGVtZW50LCBvcHRpb25zKSB7XG4gICAgICAgICAgICBvcHRpb25zID0gJC5pc1BsYWluT2JqZWN0KG9wdGlvbnMpID8gb3B0aW9ucyA6IHt9O1xuICAgICAgICAgICAgdGhpcy4kZWxlbWVudCA9ICQoZWxlbWVudCk7XG4gICAgICAgICAgICB0aGlzLmRlZmF1bHRzID0gJC5leHRlbmQoe30sIFByZXR0eURhdGUuZGVmYXVsdHMsIHRoaXMuJGVsZW1lbnQuZGF0YSgpLCBvcHRpb25zKTtcbiAgICAgICAgICAgIHRoaXMuaW5pdCgpO1xuICAgICAgICB9LFxuXG4gICAgLy8gSGVscGVyIHZhcmlhYmxlc1xuICAgICAgICBmbG9vciA9IE1hdGguZmxvb3IsXG4gICAgICAgIHNlY29uZCA9IDEwMDAsXG4gICAgICAgIG1pbnV0ZSA9IDYwICogc2Vjb25kLFxuICAgICAgICBob3VyID0gNjAgKiBtaW51dGUsXG4gICAgICAgIGRheSA9IDI0ICogaG91cixcbiAgICAgICAgd2VlayA9IDcgKiBkYXksXG4gICAgICAgIG1vbnRoID0gMzEgKiBkYXksXG4gICAgICAgIHllYXIgPSAzNjUgKiBkYXk7XG5cbiAgICBQcmV0dHlEYXRlLnByb3RvdHlwZSA9IHtcbiAgICAgICAgY29uc3RydWN0b3I6IFByZXR0eURhdGUsXG5cbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyICR0aGlzID0gdGhpcy4kZWxlbWVudCxcbiAgICAgICAgICAgICAgICBkZWZhdWx0cyA9IHRoaXMuZGVmYXVsdHMsXG4gICAgICAgICAgICAgICAgaXNJbnB1dCA9ICR0aGlzLmlzKFwiaW5wdXRcIiksXG4gICAgICAgICAgICAgICAgb3JpZ2luYWxEYXRlID0gaXNJbnB1dCA/ICR0aGlzLnZhbCgpIDogJHRoaXMudGV4dCgpO1xuXG4gICAgICAgICAgICB0aGlzLmlzSW5wdXQgPSBpc0lucHV0O1xuICAgICAgICAgICAgdGhpcy5vcmlnaW5hbERhdGUgPSBvcmlnaW5hbERhdGU7XG4gICAgICAgICAgICB0aGlzLmZvcm1hdCA9IFByZXR0eURhdGUuZm4ucGFyc2VGb3JtYXQoZGVmYXVsdHMuZGF0ZUZvcm1hdCk7XG4gICAgICAgICAgICB0aGlzLnNldERhdGUoZGVmYXVsdHMuZGF0ZSB8fCBvcmlnaW5hbERhdGUpO1xuICAgICAgICAgICAgdGhpcy5hY3RpdmUgPSB0cnVlO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5kYXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wcmV0dGlmeSgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGRlZmF1bHRzLmF1dG9VcGRhdGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0RGF0ZTogZnVuY3Rpb24gKGRhdGUpIHtcbiAgICAgICAgICAgIGlmIChkYXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRlID0gUHJldHR5RGF0ZS5mbi5wYXJzZURhdGUoZGF0ZSwgdGhpcy5mb3JtYXQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHByZXR0aWZ5OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgZGlmZiA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCkgLSB0aGlzLmRhdGUuZ2V0VGltZSgpLFxuICAgICAgICAgICAgICAgIHBhc3QgPSBkaWZmID4gMCA/IHRydWUgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlcyA9IHRoaXMuZGVmYXVsdHMubWVzc2FnZXMsXG4gICAgICAgICAgICAgICAgJHRoaXMgPSB0aGlzLiRlbGVtZW50LFxuICAgICAgICAgICAgICAgIHByZXR0eURhdGU7XG5cbiAgICAgICAgICAgIGlmICghdGhpcy5hY3RpdmUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGRpZmYgPSBkaWZmIDwgMCA/IChzZWNvbmQgLSBkaWZmKSA6IGRpZmY7XG4gICAgICAgICAgICBwcmV0dHlEYXRlID0gKFxuICAgICAgICAgICAgICAgIGRpZmYgPCAyICogc2Vjb25kID8gbWVzc2FnZXMuc2Vjb25kIDpcbiAgICAgICAgICAgICAgICAgICAgZGlmZiA8IG1pbnV0ZSA/IG1lc3NhZ2VzLnNlY29uZHMucmVwbGFjZShcIiVzXCIsIGZsb29yKGRpZmYgLyBzZWNvbmQpKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICBkaWZmIDwgMiAqIG1pbnV0ZSA/IG1lc3NhZ2VzLm1pbnV0ZSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlmZiA8IGhvdXIgPyBtZXNzYWdlcy5taW51dGVzLnJlcGxhY2UoXCIlc1wiLCBmbG9vcihkaWZmIC8gbWludXRlKSkgOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaWZmIDwgMiAqIGhvdXIgPyBtZXNzYWdlcy5ob3VyIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpZmYgPCBkYXkgPyBtZXNzYWdlcy5ob3Vycy5yZXBsYWNlKFwiJXNcIiwgZmxvb3IoZGlmZiAvIGhvdXIpKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlmZiA8IDIgKiBkYXkgPyAocGFzdCA/IG1lc3NhZ2VzLnllc3RlcmRheSA6IG1lc3NhZ2VzLnRvbW9ycm93KSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpZmYgPCAzICogZGF5ID8gKHBhc3QgPyBtZXNzYWdlcy5iZWZvcmVZZXN0ZXJkYXkgOiBtZXNzYWdlcy5hZnRlclRvbW9ycm93KSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKiBkaWZmIDwgMiAqIGRheSA/IG1lc3NhZ2VzLmRheSA6ICovXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaWZmIDwgd2VlayA/IG1lc3NhZ2VzLmRheXMucmVwbGFjZShcIiVzXCIsIGZsb29yKGRpZmYgLyBkYXkpKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlmZiA8IDIgKiB3ZWVrID8gbWVzc2FnZXMud2VlayA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpZmYgPCA0ICogd2VlayA/IG1lc3NhZ2VzLndlZWtzLnJlcGxhY2UoXCIlc1wiLCBmbG9vcihkaWZmIC8gd2VlaykpIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpZmYgPCAyICogbW9udGggPyBtZXNzYWdlcy5tb250aCA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlmZiA8IHllYXIgPyBtZXNzYWdlcy5tb250aHMucmVwbGFjZShcIiVzXCIsIGZsb29yKGRpZmYgLyBtb250aCkpIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlmZiA8IDIgKiB5ZWFyID8gbWVzc2FnZXMueWVhciA6IG1lc3NhZ2VzLnllYXJzLnJlcGxhY2UoXCIlc1wiLCBmbG9vcihkaWZmIC8geWVhcikpXG4gICAgICAgICAgICApO1xuXG5cbiAgICAgICAgICAgIGlmICh0aGlzLmlzSW5wdXQpIHtcbiAgICAgICAgICAgICAgICAkdGhpcy52YWwocHJldHR5RGF0ZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICR0aGlzLnRleHQocHJldHR5RGF0ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMucHJldHR5RGF0ZSA9IHByZXR0eURhdGU7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyICR0aGlzID0gdGhpcy4kZWxlbWVudCxcbiAgICAgICAgICAgICAgICBvcmlnaW5hbERhdGUgPSB0aGlzLm9yaWdpbmFsRGF0ZTtcblxuICAgICAgICAgICAgaWYgKCF0aGlzLmFjdGl2ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuZGVmYXVsdHMuYXV0b1VwZGF0ZSAmJiB0aGlzLmF1dG9VcGRhdGUpIHtcbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuYXV0b1VwZGF0ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmlzSW5wdXQpIHtcbiAgICAgICAgICAgICAgICAkdGhpcy52YWwob3JpZ2luYWxEYXRlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHRoaXMudGV4dChvcmlnaW5hbERhdGUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkdGhpcy5yZW1vdmVEYXRhKFwicHJldHR5ZGF0ZVwiKTtcblxuICAgICAgICAgICAgdGhpcy5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBkdXJhdGlvbiA9IHRoaXMuZGVmYXVsdHMuZHVyYXRpb24sXG4gICAgICAgICAgICAgICAgdGhhdCA9IHRoaXM7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgZHVyYXRpb24gPT09IFwibnVtYmVyXCIgJiYgZHVyYXRpb24gPiAwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hdXRvVXBkYXRlID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB0aGF0LnByZXR0aWZ5KCk7XG4gICAgICAgICAgICAgICAgfSwgZHVyYXRpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcblxuICAgIFByZXR0eURhdGUuZm4gPSB7XG4gICAgICAgIHBhcnNlRm9ybWF0OiBmdW5jdGlvbiAoZm9ybWF0KSB7XG4gICAgICAgICAgICB2YXIgcGFydHMgPSB0eXBlb2YgZm9ybWF0ID09PSBcInN0cmluZ1wiID8gZm9ybWF0Lm1hdGNoKC8oXFx3KykvZykgOiBbXSxcbiAgICAgICAgICAgICAgICBtb250aE1hdGNoZWQgPSBmYWxzZTtcblxuICAgICAgICAgICAgaWYgKCFwYXJ0cyB8fCBwYXJ0cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIGRhdGUgZm9ybWF0LlwiKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZm9ybWF0ID0gJC5tYXAocGFydHMsIGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBhcnQgPSBuLnN1YnN0cigwLCAxKTtcblxuICAgICAgICAgICAgICAgIHN3aXRjaCAocGFydCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiU1wiOlxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwic1wiOlxuICAgICAgICAgICAgICAgICAgICAgICAgcGFydCA9IFwic1wiO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIm1cIjpcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcnQgPSBtb250aE1hdGNoZWQgPyBcIm1cIiA6IFwiTVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgbW9udGhNYXRjaGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJIXCI6XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJoXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJ0ID0gXCJoXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiRFwiOlxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiZFwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgcGFydCA9IFwiRFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIk1cIjpcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcnQgPSBcIk1cIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vbnRoTWF0Y2hlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwiWVwiOlxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwieVwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgcGFydCA9IFwiWVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gTm8gZGVmYXVsdFxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiBwYXJ0O1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJldHVybiBmb3JtYXQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcGFyc2VEYXRlOiBmdW5jdGlvbiAoZGF0ZSwgZm9ybWF0KSB7XG4gICAgICAgICAgICB2YXIgcGFydHMgPSB0eXBlb2YgZGF0ZSA9PT0gXCJzdHJpbmdcIiA/IGRhdGUubWF0Y2goLyhcXGQrKS9nKSA6IFtdLFxuICAgICAgICAgICAgICAgIGRhdGEgPSB7XG4gICAgICAgICAgICAgICAgICAgIFk6IDAsXG4gICAgICAgICAgICAgICAgICAgIE06IDAsXG4gICAgICAgICAgICAgICAgICAgIEQ6IDAsXG4gICAgICAgICAgICAgICAgICAgIGg6IDAsXG4gICAgICAgICAgICAgICAgICAgIG06IDAsXG4gICAgICAgICAgICAgICAgICAgIHM6IDBcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZiAoJC5pc0FycmF5KHBhcnRzKSAmJiAkLmlzQXJyYXkoZm9ybWF0KSAmJiBwYXJ0cy5sZW5ndGggPT09IGZvcm1hdC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAkLmVhY2goZm9ybWF0LCBmdW5jdGlvbiAoaSwgbikge1xuICAgICAgICAgICAgICAgICAgICBkYXRhW25dID0gcGFyc2VJbnQocGFydHNbaV0sIDEwKSB8fCAwO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgZGF0YS5ZICs9IGRhdGEuWSA+IDAgJiYgZGF0YS5ZIDwgMTAwID8gMjAwMCA6IDA7IC8vIFllYXI6IDE0IC0+IDIwMTRcblxuICAgICAgICAgICAgICAgIGRhdGUgPSBuZXcgRGF0ZShkYXRhLlksIGRhdGEuTSAtIDEsIGRhdGEuRCwgZGF0YS5oLCBkYXRhLm0sIGRhdGEucyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRhdGUgPSBuZXcgRGF0ZShkYXRlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGRhdGUuZ2V0VGltZSgpID8gZGF0ZSA6IG51bGw7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgUHJldHR5RGF0ZS5kZWZhdWx0cyA9IHtcbiAgICAgICAgYXV0b1VwZGF0ZTogZmFsc2UsXG4gICAgICAgIGRhdGU6IG51bGwsXG4gICAgICAgIGRhdGVGb3JtYXQ6IFwiWVlZWS1NTS1ERCBoaDptbTpzc1wiLFxuICAgICAgICBkdXJhdGlvbjogNjAwMDAsIC8vIG1pbGxpc2Vjb25kc1xuICAgICAgICBtZXNzYWdlczoge1xuICAgICAgICAgICAgc2Vjb25kOiBcInByw6F2xJsgdGXEj1wiLFxuICAgICAgICAgICAgc2Vjb25kczogXCJwxZllZCAlcyBzZWt1bmRhbWlcIixcbiAgICAgICAgICAgIG1pbnV0ZTogXCJwxZllZCBtaW51dG91XCIsXG4gICAgICAgICAgICBtaW51dGVzOiBcInDFmWVkICVzIG1pbnV0YW1pXCIsXG4gICAgICAgICAgICBob3VyOiBcInDFmWVkIGhvZGlub3VcIixcbiAgICAgICAgICAgIGhvdXJzOiBcInDFmWVkICVzIGhvZGluYW1pXCIsXG4gICAgICAgICAgICBkYXk6IFwidsSNZXJhXCIsXG4gICAgICAgICAgICBkYXlzOiBcInDFmWVkICVzIGRueVwiLFxuICAgICAgICAgICAgd2VlazogXCJwxZllZCB0w71kbmVtXCIsXG4gICAgICAgICAgICB3ZWVrczogXCJwxZllZCAlcyB0w71kbnlcIixcbiAgICAgICAgICAgIG1vbnRoOiBcInDFmWVkIG3Em3PDrWNlbVwiLFxuICAgICAgICAgICAgbW9udGhzOiBcInDFmWVkICVzIG3Em3PDrWNpXCIsXG4gICAgICAgICAgICB5ZWFyOiBcInDFmWVkIHJva2VtXCIsXG4gICAgICAgICAgICB5ZWFyczogXCJwxZllZCAlcyBsZXR5XCIsXG5cbiAgICAgICAgICAgIC8vIEV4dHJhXG4gICAgICAgICAgICB5ZXN0ZXJkYXk6IFwidsSNZXJhXCIsXG4gICAgICAgICAgICBiZWZvcmVZZXN0ZXJkYXk6IFwicMWZZWQgMiBkbnlcIixcbiAgICAgICAgICAgIHRvbW9ycm93OiBcInrDrXRyYVwiLFxuICAgICAgICAgICAgYWZ0ZXJUb21vcnJvdzogXCJwb3rDrXTFmcOtXCJcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBQcmV0dHlEYXRlLnNldERlZmF1bHRzID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgJC5leHRlbmQoUHJldHR5RGF0ZS5kZWZhdWx0cywgb3B0aW9ucyk7XG4gICAgfTtcblxuICAgIC8vIFJlZ2lzdGVyIGFzIGpRdWVyeSBwbHVnaW5cbiAgICAkLmZuLnByZXR0eWRhdGUgPSBmdW5jdGlvbiAob3B0aW9ucywgc2V0dGluZ3MpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgIGRhdGEgPSAkdGhpcy5kYXRhKFwicHJldHR5ZGF0ZVwiKTtcblxuICAgICAgICAgICAgaWYgKCFkYXRhKSB7XG4gICAgICAgICAgICAgICAgJHRoaXMuZGF0YShcInByZXR0eWRhdGVcIiwgKGRhdGEgPSBuZXcgUHJldHR5RGF0ZSh0aGlzLCBvcHRpb25zKSkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIG9wdGlvbnMgPT09IFwic3RyaW5nXCIgJiYgJC5pc0Z1bmN0aW9uKGRhdGFbb3B0aW9uc10pKSB7XG4gICAgICAgICAgICAgICAgZGF0YVtvcHRpb25zXShzZXR0aW5ncyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICAkLmZuLnByZXR0eWRhdGUuY29uc3RydWN0b3IgPSBQcmV0dHlEYXRlO1xuICAgICQuZm4ucHJldHR5ZGF0ZS5zZXREZWZhdWx0cyA9IFByZXR0eURhdGUuc2V0RGVmYXVsdHM7XG5cbiAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJChcIltwcmV0dHlkYXRlXVwiKS5wcmV0dHlkYXRlKCk7XG4gICAgfSk7XG59KTtcbiIsIiQoZnVuY3Rpb24gKCkge1xuXG4gICAgJChcIi5pbmxpbmUtbGFiZWxfX2lucHV0XCIpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XG4gICAgICAgIHZhciAkaW5wdXQgPSAkKGVsZW1lbnQpO1xuICAgICAgICB2YXIgJGxhYmVsID0gJCh0aGlzKS5uZXh0KCk7XG4gICAgICAgIHZhciAkcGFyZW50ID0gJCh0aGlzKS5wYXJlbnQoKS5lcSgwKTtcbiAgICAgICAgdmFyICRjbGFzcyA9IFwianMtZmlsbGVkLWlucHV0XCI7XG5cbiAgICAgICAgaWYgKCRpbnB1dC52YWwoKS5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgJGxhYmVsLnJlbW92ZUNsYXNzKCRjbGFzcyk7XG4gICAgICAgIH1cblxuICAgICAgICAkcGFyZW50Lm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGlucHV0LmZvY3VzKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRpbnB1dC5vbihcImZvY3VzXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgJGxhYmVsLmFkZENsYXNzKCRjbGFzcyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRpbnB1dC5vbihcImJsdXIgY2hhbmdlXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBpZiAoJGlucHV0LnZhbCgpLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgJGxhYmVsLnJlbW92ZUNsYXNzKCRjbGFzcyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRsYWJlbC5hZGRDbGFzcygkY2xhc3MpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcblxufSk7Il19
